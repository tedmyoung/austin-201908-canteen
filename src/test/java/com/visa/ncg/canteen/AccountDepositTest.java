package com.visa.ncg.canteen;

import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.InvalidAmountException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AccountDepositTest {

  @Test
  public void newAccountHasBalanceOfZero() throws Exception {
    Account account = new Account();

    assertThat(account.balance())
        .isZero();
  }

  @Test
  public void deposit49ResultsInBalanceOf49() throws Exception {
    Account account = new Account();

    account.deposit(49);

    assertThat(account.balance())
        .isEqualTo(49);
  }

  @Test
  public void deposit77And42ResultsInBalanceOf119() throws Exception {
    Account account = new Account();

    account.deposit(77);
    account.deposit(42);

    assertThat(account.balance())
        .isEqualTo(119);
  }

  @Test
  public void deposit0ThrowsInvalidAmountException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> {
      account.deposit(0);
    })
        .isInstanceOf(InvalidAmountException.class);
  }

  @Test
  public void depositNegative5ThrowsInvalidAmountException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> {
      account.deposit(-5);
    })
        .isInstanceOf(InvalidAmountException.class);
  }
}
