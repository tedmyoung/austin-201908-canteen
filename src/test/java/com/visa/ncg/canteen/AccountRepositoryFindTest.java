package com.visa.ncg.canteen;

import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.AccountRepository;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountRepositoryFindTest {

  @Test
  public void findAllOnNewRepositoryReturnsEmptyList() throws Exception {
    AccountRepository accountRepository = new FakeAccountRepository();

    assertThat(accountRepository.findAll())
        .isEmpty();
  }

  @Test
  public void findAllOnNewRepositoryWithOneAccountFindsIt() throws Exception {
    Account account = new Account();
    account.setId(1L);
    AccountRepository accountRepository = new FakeAccountRepository();
    accountRepository.save(account);

    assertThat(accountRepository.findAll())
        .hasSize(1)
        .extracting("id")
        .containsOnly(1L);
  }

  @Test
  public void findAllNewRepositoryWith3AccountsFindsAll3() throws Exception {
    AccountRepository accountRepository = new FakeAccountRepository();
    Account account1 = new Account();
    account1.setId(1L);
    accountRepository.save(account1);
    Account account2 = new Account();
    account2.setId(2L);
    accountRepository.save(account2);

    assertThat(accountRepository.findAll())
        .hasSize(2)
        .extracting("id")
        .containsOnly(1L, 2L);
  }

  @Test
  public void findOneNewEmptyRepositoryReturnsNull() throws Exception {
    AccountRepository accountRepository = new FakeAccountRepository();

    assertThat(accountRepository.findOne(1L))
        .isNull();
  }

  @Test
  public void findOneForExistingIdReturnsThatAccount() throws Exception {
    AccountRepository accountRepository = new FakeAccountRepository();
    Account account = new Account();
    account.setId(7L);
    accountRepository.save(account);

    assertThat(accountRepository.findOne(7L).getId())
        .isEqualTo(7L);
  }

  @Test
  public void findOneWhenMultipleAccountsReturnsMatchingAccount() throws Exception {
    AccountRepository accountRepository = new FakeAccountRepository();
    Account account1 = new Account();
    account1.setId(8L);
    accountRepository.save(account1);
    Account account2 = new Account();
    account2.setId(9L);
    accountRepository.save(account2);

    assertThat(accountRepository.findOne(9L).getId())
        .isEqualTo(9L);
  }
}
