package com.visa.ncg.canteen;

import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.InsufficientBalanceException;
import com.visa.ncg.canteen.domain.InvalidAmountException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AccountWithdrawTest {

  @Test
  public void withdraw10FromBalanceOf13ResultsInBalanceOf3() throws Exception {
    Account account = new Account();
    account.deposit(13);

    account.withdraw(10);

    assertThat(account.balance())
        .isEqualTo(3);
  }

  @Test
  public void withdraw19AndThen11FromBalanceOf50ResultsIn20() throws Exception {
    Account account = new Account();
    account.deposit(50);

    account.withdraw(19);
    account.withdraw(11);

    assertThat(account.balance())
        .isEqualTo(20);
  }

  @Test
  public void withdraw0ThrowsInvalidAmountException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> {
      account.withdraw(0);
    })
        .isInstanceOf(InvalidAmountException.class);
  }

  @Test
  public void withdrawNegative13ThrowsInvalidAmountException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> {
      account.withdraw(-13);
    })
        .isInstanceOf(InvalidAmountException.class);
  }

  @Test
  public void withdraw43FromBalanceOf32ThrowsInsufficientBalanceException() throws Exception {
    Account account = new Account();
    account.deposit(32);

    assertThatThrownBy(() -> {
      account.withdraw(43);
    })
        .isInstanceOf(InsufficientBalanceException.class);
  }

}
