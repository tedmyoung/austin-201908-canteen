package com.visa.ncg.canteen.adapter.api;

import com.visa.ncg.canteen.adapter.web.AccountResponse;
import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountApiController {
  private final AccountRepository accountRepository;

  @Autowired
  public AccountApiController(AccountRepository accountRepository) {
    this.accountRepository = accountRepository;
  }

  @GetMapping("/api/accounts/{id}")
  public AccountResponse accountInfo(@PathVariable("id") String accountId) {
    Long id = Long.valueOf(accountId);

    Account account = accountRepository.findOne(id);

    return AccountResponse.from(account);
  }

  @PostMapping("/api/accounts")
  public AccountResponse createAccount(@RequestBody AccountCreateRequest accountCreateRequest) {
    Account account = new Account();
    account.deposit(accountCreateRequest.getInitialBalance());
    account.changeNameTo(accountCreateRequest.getAccountName());

    account = accountRepository.save(account);

    return AccountResponse.from(account);
  }
}
