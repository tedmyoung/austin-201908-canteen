package com.visa.ncg.canteen.adapter.jpa;

import org.springframework.data.repository.CrudRepository;

public interface AccountJpaRepository extends CrudRepository<AccountDto, Long> {
}
