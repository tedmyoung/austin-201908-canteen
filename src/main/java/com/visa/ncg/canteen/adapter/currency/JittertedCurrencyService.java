package com.visa.ncg.canteen.adapter.currency;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Primary
@Service
public class JittertedCurrencyService implements CurrencyService {

  private static final String CURRENCY_CONVERSION_URL =
      "http://jitterted-currency-conversion.herokuapp.com/convert?from={from}&to={to}&amount={amount}";

  private final RestTemplate restTemplate = new RestTemplate();

  @Override
  public int convertToGbp(int amount) {
    Map<String, String> params = createParameterMap(amount, "GBP");

    ConvertedCurrency convertedCurrency = restTemplate
        .getForObject(CURRENCY_CONVERSION_URL, ConvertedCurrency.class, params);

    return (int) convertedCurrency.getConverted();
  }


  private Map<String, String> createParameterMap(int amount, String targetCurrency) {
    Map<String, String> params = new HashMap<>();
    params.put("from", "USD");
    params.put("to", targetCurrency);
    params.put("amount", Integer.toString(amount));
    return params;
  }

}
