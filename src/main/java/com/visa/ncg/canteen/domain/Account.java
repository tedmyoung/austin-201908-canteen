package com.visa.ncg.canteen.domain;

public class Account {
  private Long id;

  private int balance = 0;
  private String name;

  public int balance() {
    return balance;
  }

  public void deposit(int amount) {
    ensureValidAmount(amount);
    balance += amount;
  }

  public void withdraw(int amount) {
    ensureValidAmount(amount);
    ensureSufficientBalance(amount);
    balance -= amount;
  }

  private void ensureSufficientBalance(int amount) {
    if (balance < amount) {
      throw new InsufficientBalanceException();
    }
  }

  private void ensureValidAmount(int amount) {
    if (amount <= 0) {
      throw new InvalidAmountException();
    }
  }

  public String name() {
    return name;
  }

  public void changeNameTo(String newName) {
    name = newName;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
