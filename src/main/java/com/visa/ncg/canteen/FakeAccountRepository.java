package com.visa.ncg.canteen;

import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.AccountRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@Repository
public class FakeAccountRepository implements AccountRepository {

  private final Map<Long, Account> accountMap = new HashMap<>();
  private final AtomicLong sequence = new AtomicLong(0);

  public Account findOne(Long id) {
    return accountMap.get(id);
  }

  public Account save(Account entity) {
    assignIdIfNull(entity);
    accountMap.put(entity.getId(), entity);
    return entity;
  }

  private void assignIdIfNull(Account entity) {
    if (entity.getId() == null) {
      entity.setId(sequence.getAndIncrement());
    }
  }

  public List<Account> findAll() {
    return new ArrayList<>(accountMap.values());
  }

}